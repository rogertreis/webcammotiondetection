const video = document.getElementById("myvideo");
const canvas = document.getElementById("canvas");
const poscanvas = document.getElementById("poscanvas");
const context = canvas.getContext("2d");
const sqrsize = 50;
var values = 0;
let trackButton = document.getElementById("trackbutton");
let updateNote = document.getElementById("updatenote");

let isVideo = false;
let model = null;

const modelParams = {
    flipHorizontal: false,   // flip e.g for video  
    maxNumBoxes: 10,        // maximum number of boxes to detect
    iouThreshold: 0.5,      // ioU threshold for non-max suppression
    scoreThreshold: 0.7,    // confidence threshold for predictions.
}

function startVideo() {
    handTrack.startVideo(video).then(function (status) {
        console.log("video iniciado", status);
        if (status) {
            updateNote.innerText = "Video iniciado. Rastreando"
            isVideo = true
            runDetection()
        } else {
            updateNote.innerText = "Por favor, libere o video"
        }
    });
}

function toggleVideo() {
    if (!isVideo) {
        updateNote.innerText = "Iniciando video"
        startVideo();
    } else {
        updateNote.innerText = "Parando video"
        handTrack.stopVideo(video)
        isVideo = false;
        updateNote.innerText = "Video parado"
    }
}



function runDetection() {
    model.detect(video).then(predictions => {
        console.log("Predictions: ", predictions);
        values = predictions;
        model.renderPredictions(predictions, canvas, context, video);
        if (isVideo) {
            requestAnimationFrame(drawBoundBox);
            requestAnimationFrame(runDetection);
        }
    });
}

function drawBoundBox() {
    var ctx = poscanvas.getContext("2d");
    var centerX = poscanvas.width / 2;
    var centerY = poscanvas.height / 2;
    ctx.beginPath();
    
    if (overlaps()) {
        ctx.strokeStyle = "green";
    } else {
        ctx.strokeStyle = "red";
    }
    
    ctx.rect(centerX - sqrsize / 2, centerY - sqrsize / 2, sqrsize, sqrsize);
    ctx.stroke();
}

function overlaps() {
    var bx1 = (poscanvas.width / 2) - (sqrsize / 2);
    var bx2 = bx1 + sqrsize;
    var by1 = (poscanvas.height / 2) - (sqrsize / 2);
    var by2 = by1 + sqrsize;

    var ax1 = values[0].bbox[0];
    var ax2 = values[0].bbox[0] + values[0].bbox[2];
    var ay1 = values[0].bbox[1];
    var ay2 = values[0].bbox[1] + values[0].bbox[3];

	// no horizontal overlap
	if (ax1 >= bx2 || bx1 >= ax2) return false;

	// no vertical overlap
	if (ay1 >= by2 || by1 >= ay2) return false;

	return true;
}

//rectangle.prototype.intersects = function(rect) {
//     return !( rect.x           > (this.x + this.w) || 
//              (rect.x + rect.w) <  this.x           || 
//               rect.y           > (this.y + this.h) ||
//              (rect.y + rect.h) <  this.y);
// }

// Load the model.
handTrack.load(modelParams).then(lmodel => {
    // detect objects in the image.
    model = lmodel
    updateNote.innerText = "Loaded Model!"
    trackButton.disabled = false
});